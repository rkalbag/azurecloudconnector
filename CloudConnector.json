{
	"$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
	"contentVersion": "1.0.0.0",
	"parameters": {
		"vmName": {
			"type": "string",
			"defaultValue": "simpleLinuxVM",
			"metadata": {
				"description": "The name of you Virtual Machine."
			}
		},
		"adminUsername": {
			"type": "string",
			"metadata": {
				"description": "Username for the Virtual Machine."
			}
		},
		"authenticationType": {
			"type": "string",
			"defaultValue": "password",
			"allowedValues": [
				"sshPublicKey",
				"password"
			],
			"metadata": {
				"description": "Type of authentication to use on the Virtual Machine. SSH key is recommended."
			}
		},
		"adminPasswordOrKey": {
			"type": "securestring",
			"metadata": {
				"description": "SSH Key or password for the Virtual Machine. SSH key is recommended."
			}
		},
		"connectorToken": {
			"type": "securestring",
			"metadata": {
				"description": "The token presented on OpenVPN Cloud Admin Portal"
			}
		},
		"dnsLabelPrefix": {
			"type": "string",
			"defaultValue": "[toLower(concat('cc-', uniqueString(resourceGroup().id)))]",
			"metadata": {
				"description": "Unique DNS Name for the Public IP used to access the Virtual Machine."
			}
		},
		"location": {
			"type": "string",
			"defaultValue": "[resourceGroup().location]",
			"metadata": {
				"description": "Location for all resources."
			}
		},
		"VmSize": {
			"type": "string",
			"defaultValue": "Standard_B1s",
			"metadata": {
				"description": "The size of the VM"
			}
		},
		"virtualNetworkName": {
			"type": "string",
			"defaultValue": "vNet",
			"metadata": {
				"description": "Name of the VNET"
			}
		},
    "addressPrefix": {
      "type": "string",
      "defaultValue": "10.1.0.0/16",
      "metadata": {
        "description": "IP address range of the VNet."
      }
    },

		"subnetName": {
			"type": "string",
			"defaultValue": "Subnet",
			"metadata": {
				"description": "Name of the subnet in the virtual network"
			}
		},
    "subnetAddressPrefix": {
      "type": "string",
      "defaultValue": "10.1.0.0/24",
      "metadata": {
        "description": "IP address range of the subnet"
      }
    },
		"networkSecurityGroupName": {
			"type": "string",
			"defaultValue": "SecGroupNet",
			"metadata": {
				"description": "Name of the Network Security Group"
			}
		}
	},
	"variables": {
		"publicIpAddressName": "[concat(parameters('vmName'), 'PublicIP' )]",
		"networkInterfaceName": "[concat(parameters('vmName'),'NetInt')]",
		"subnetRef": "[resourceId('Microsoft.Network/virtualNetworks/subnets', parameters('virtualNetworkName'), parameters('subnetName'))]",
		"osDiskType": "Standard_LRS",
    "openvpn3Script": "[concat('#!/bin/bash\n','wget https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub\n','sudo apt-key add openvpn-repo-pkg-key.pub && rm -f openvpn-repo-pkg-key.pub\n','sudo wget -O /etc/apt/sources.list.d/openvpn3.list https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-focal.list\n','sudo apt --assume-yes update\n','sudo apt --assume-yes install python3-openvpn-connector-setup\n')]",
    "clientStart": "[concat('sudo openvpn-connector-setup --token ', parameters('connectorToken'), '\n')]",
    "ipFwd": "sudo sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf\nsudo sysctl -p\n",
    "nat": "echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections\necho iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections\nsudo apt-get -y install iptables-persistent\nsudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE\nsudo iptables-save > /etc/iptables/rules.v4\n",
		"customData": "[concat(variables('openvpn3Script'), variables('ipFwd'), variables('clientStart'))]",
		"linuxConfiguration": {
			"disablePasswordAuthentication": true,
			"ssh": {
				"publicKeys": [{
					"path": "[concat('/home/', parameters('adminUsername'), '/.ssh/authorized_keys')]",
					"keyData": "[parameters('adminPasswordOrKey')]"
				}]
			}
		}
	},
	"resources": [{
			"type": "Microsoft.Network/networkInterfaces",
			"apiVersion": "2018-10-01",
			"name": "[variables('networkInterfaceName')]",
			"location": "[parameters('location')]",
			"dependsOn": [
				"[resourceId('Microsoft.Network/networkSecurityGroups/', parameters('networkSecurityGroupName'))]",
				"[resourceId('Microsoft.Network/virtualNetworks/', parameters('virtualNetworkName'))]",
				"[resourceId('Microsoft.Network/publicIpAddresses/', variables('publicIpAddressName'))]"
			],
			"properties": {
				"enableIPForwarding": true,
				"ipConfigurations": [{
					"name": "ipconfig1",
					"properties": {
						"subnet": {
							"id": "[variables('subnetRef')]"
						},
						"privateIPAllocationMethod": "Dynamic",
						"publicIpAddress": {
							"id": "[resourceId('Microsoft.Network/publicIPAddresses',variables('publicIPAddressName'))]"
						}
					}
				}],
				"networkSecurityGroup": {
					"id": "[resourceId('Microsoft.Network/networkSecurityGroups',parameters('networkSecurityGroupName'))]"
				}
			}
		},
		{
			"type": "Microsoft.Network/networkSecurityGroups",
			"apiVersion": "2019-02-01",
			"name": "[parameters('networkSecurityGroupName')]",
			"location": "[parameters('location')]",
			"properties": {
				"securityRules": [{
					"name": "SSH",
					"properties": {
						"priority": 1000,
						"protocol": "TCP",
						"access": "Allow",
						"direction": "Inbound",
						"sourceAddressPrefix": "*",
						"sourcePortRange": "*",
						"destinationAddressPrefix": "*",
						"destinationPortRange": "22"
					}
				}]
			}
		},
		{
			"type": "Microsoft.Network/virtualNetworks",
			"apiVersion": "2019-04-01",
			"name": "[parameters('virtualNetworkName')]",
			"location": "[parameters('location')]",
			"properties": {
				"addressSpace": {
					"addressPrefixes": [
						"[parameters('addressPrefix')]"
					]
				},
				"subnets": [{
					"name": "[parameters('subnetName')]",
					"properties": {
						"addressPrefix": "[parameters('subnetAddressPrefix')]",
						"privateEndpointNetworkPolicies": "Enabled",
						"privateLinkServiceNetworkPolicies": "Enabled"
					}
				}]
			}
		},
		{
			"type": "Microsoft.Network/publicIpAddresses",
			"apiVersion": "2019-02-01",
			"name": "[variables('publicIpAddressName')]",
			"location": "[parameters('location')]",
			"properties": {
				"publicIpAllocationMethod": "Static",
				"publicIPAddressVersion": "IPv4",
				"dnsSettings": {
					"domainNameLabel": "[parameters('dnsLabelPrefix')]"
				},
				"idleTimeoutInMinutes": 10
			},
			"sku": {
				"name": "Basic",
				"tier": "Regional"
			}
		},
		{
			"type": "Microsoft.Compute/virtualMachines",
			"apiVersion": "2019-03-01",
			"name": "[parameters('vmName')]",
			"location": "[parameters('location')]",
			"dependsOn": [
				"[resourceId('Microsoft.Network/networkInterfaces/', variables('networkInterfaceName'))]"
			],
			"properties": {
				"hardwareProfile": {
					"vmSize": "[parameters('VmSize')]"
				},
				"storageProfile": {
					"osDisk": {
						"createOption": "fromImage",
						"managedDisk": {
							"storageAccountType": "[variables('osDiskType')]"
						}
					},
					"imageReference": {
						"publisher": "canonical",
						"offer": "0001-com-ubuntu-server-focal",
						"sku": "20_04-lts",
						"version": "latest"
					}
				},
				"networkProfile": {
					"networkInterfaces": [{
						"id": "[resourceId('Microsoft.Network/networkInterfaces', variables('networkInterfaceName'))]"
					}]
				},
				"osProfile": {
					"computerName": "[parameters('vmName')]",
					"adminUsername": "[parameters('adminUsername')]",
					"adminPassword": "[parameters('adminPasswordOrKey')]",
					"customData": "[base64(variables('customData'))]",
					"linuxConfiguration": "[if(equals(parameters('authenticationType'), 'password'), json('null'), variables('linuxConfiguration'))]"
				}
			}
		}
	],
	"outputs": {
		"adminUsername": {
			"type": "string",
			"value": "[parameters('adminUsername')]"
		},
		"hostname": {
			"type": "string",
			"value": "[reference(variables('publicIPAddressName')).dnsSettings.fqdn]"
		},
		"sshCommand": {
			"type": "string",
			"value": "[concat('ssh ', parameters('adminUsername'), '@', reference(variables('publicIPAddressName')).dnsSettings.fqdn)]"
		}
	}
}
